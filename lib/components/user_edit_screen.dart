import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:math';
import 'package:path/path.dart' as Path;
import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:toast/toast.dart';

class UserEditScreenPage extends StatefulWidget {
  Map<dynamic, dynamic> list ;
  String userKey;
  
  UserEditScreenPage({Key key, @required this.userKey, @required this.list}) : super(key: key);

  @override
  _UserEditScreenPageState createState() =>  _UserEditScreenPageState(list, userKey);
}

class _UserEditScreenPageState extends State<UserEditScreenPage> {
  final databaseReference = FirebaseDatabase.instance.reference();
  Map<dynamic, dynamic>  list;
  _UserEditScreenPageState(this.list, this.userKey);
  String userKey;
  TextEditingController _userNameController = TextEditingController();
  TextEditingController _userAgeController = TextEditingController();
  TextEditingController _userHobbiesController = TextEditingController();
  TextEditingController _userLocationController = TextEditingController();

  File _image;
  String _currentLocation;

  @override
  void initState() {
    super.initState();
    _userNameController = new TextEditingController(text: list["name"]);
    _userAgeController = new TextEditingController(text: list["age"]);
    _userHobbiesController = new TextEditingController(text: list["hobbies"]);
    if (_currentLocation == null) {
      _userLocationController =
      new TextEditingController(text: list["location"]);
    }else{
      _userLocationController =
      new TextEditingController(text: _currentLocation);
    }
  }

  _getCurrentLocation() async {
    await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
        .then((Position position) async {
      final coordinates = new Coordinates(
          position.latitude, position.longitude);
      var addresses = await Geocoder.local.findAddressesFromCoordinates(
          coordinates);
      var first = addresses.first;
      setState(() {
        _currentLocation = '${first.locality}';
      });
    }).catchError((e) {
      print(e);
    });
  }

  _getImage() async {
    ImagePicker picker = ImagePicker();
    final pickedFileCamera = await picker.getImage(source: ImageSource.camera);
    final pickedFileGallery = await picker.getImage(
        source: ImageSource.gallery);
    setState(() {
      if (pickedFileCamera != null) {
        _image = File(pickedFileCamera.path);
      } else if (pickedFileGallery != null) {
        _image = File(pickedFileGallery.path);
      } else {
        print('No image selected.');
      }
    });
  }

  _uploadFile() async {

    if (_image == null){
      updateData(list["imageURL"].toString());
    }else{
      firebase_storage.Reference storageReference = firebase_storage
          .FirebaseStorage.instance
          .ref()
          .child('${Path.basename(_image.path)}');
      firebase_storage.UploadTask uploadTask = storageReference.putFile(_image);
      firebase_storage.TaskSnapshot taskSnapshot = await uploadTask;
      String _uploadedFileURL = await taskSnapshot.ref.getDownloadURL();
      updateData(_uploadedFileURL);
    }
  }

  void updateData(_uploadedFileURL) {
    if (_userNameController.text
        .trim()
        .isEmpty || _userAgeController.text
        .toString()
        .isEmpty || _userLocationController.text
        .trim()
        .isEmpty || _uploadedFileURL
        .toString()
        .isEmpty || _userHobbiesController.text.isEmpty) {
      Toast.show("Invalid Entries.", context, duration: Toast.LENGTH_LONG,
          gravity: Toast.BOTTOM);
    } else {
      databaseReference.child("Users").child(userKey).set({
        'name': _userNameController.text.trim(),
        'age': _userAgeController.text.trim(),
        'location': _userLocationController.text.trim(),
        'imageURL': _uploadedFileURL,
        'hobbies': _userHobbiesController.text.trim()
      });


      Toast.show("User Updated", context, duration: Toast.LENGTH_LONG,
          gravity: Toast.BOTTOM);
      Navigator.pop(context);
    }
  }

  void deleteData (){
    databaseReference.child("Users").child(userKey).remove().then((value) => {
      Toast.show("User Deleted", context, duration: Toast.LENGTH_LONG,
      gravity: Toast.BOTTOM),

      Navigator.pop(context)
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade300,
      body: SingleChildScrollView(
        child: Stack(
          children: <Widget>[
            SizedBox(
              height: 250,
              child: Image.network(list["imageURL"],
                fit: BoxFit.cover,
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(16.0, 200.0, 16.0, 16.0),
              child: Column(
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(16.0),
                        margin: EdgeInsets.only(top: 16.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5.0)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(left: 96.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    list["name"].toString(),
                                    style: Theme.of(context).textTheme.title,
                                  ),
                                  ListTile(
                                    contentPadding: EdgeInsets.all(0),
                                    title: Text(list["hobbies"].toString()),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(height: 10.0),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  child: Column(
                                    children: <Widget>[
                                      Text(list["age"].toString()),
                                      Text("Age")
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Column(
                                    children: <Widget>[
                                      Text(list["location"].toString()),
                                      Text("Location")
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 80,
                        width: 80,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            image: DecorationImage(
                                image: NetworkImage(list["imageURL"]), fit: BoxFit.cover)),
                        margin: EdgeInsets.only(left: 16.0),
                      ),

                    ],
                  ),
                  SizedBox(height: 20.0),
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    child: Column(
                      children: <Widget>[
                        ListTile(
                          title: Text("Edit User"),
                        ),
                        Divider(),
                        ListTile(
                          title: TextField(controller: _userNameController,),
                          leading: Icon(Icons.email),

                        ),
                        ListTile(
                          title: TextField( controller: _userAgeController,),
                          leading: Icon(Icons.phone),
                        ),
                        ListTile(
                          title: TextField(controller: _userLocationController,),
                          leading: Icon(Icons.location_pin),
                          trailing: IconButton(
                            icon: Icon(Icons.location_searching, color: Colors.blue,),
                            onPressed: _getCurrentLocation,
                          ),
                        ),
                        ListTile(
                          title: TextField( controller: _userHobbiesController,),
                          leading: Icon(Icons.person),
                        ),
                        ListTile(
                          title: RaisedButton(
                            child: Container(
                              child: Center(
                                child: _image == null
                                    ? Image.network(list["imageURL"].toString())
                                    : Image.file(_image),
                              ),
                            ),
                          onPressed: _getImage,
                          ),
                          leading: Icon(Icons.image),
                        ),
                        ListTile(
                          leading: RaisedButton.icon(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(2.0)),
                            color: Colors.blue,
                            icon: Icon(
                              Icons.upload_sharp,
                              color: Colors.white,
                            ),
                            label: Text(
                              "Update",
                              style: TextStyle(color: Colors.white),
                            ),
                            onPressed: _uploadFile,
                          ),
                          trailing: RaisedButton.icon(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(2.0)),
                            color: Colors.red,
                            icon: Icon(
                              Icons.delete,
                              color: Colors.white,
                            ),
                            label: Text(
                              "Delete",
                              style: TextStyle(color: Colors.white),
                            ),
                            onPressed: deleteData,
                          ),
                        ),

                      ],
                    ),
                  ),

                ],
              ),
            ),
            AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
            )
          ],
        ),
      ),
    );
  }
}
