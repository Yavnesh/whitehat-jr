import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart';
import 'package:image_picker/image_picker.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:flutter_tagging/flutter_tagging.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:path/path.dart' as Path;
import 'package:toast/toast.dart';

import 'loading_dialog.dart';

class NewUserScreenPage extends StatefulWidget {
  @override
  _NewUserScreenPageState createState() =>  _NewUserScreenPageState();
}

class _NewUserScreenPageState extends State<NewUserScreenPage> {
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  final TextEditingController _userNameController = TextEditingController();
  final databaseReference = FirebaseDatabase.instance.reference();
  String defaultAvatar = "https://banner2.cleanpng.com/20180920/yko/kisspng-computer-icons-portable-network-graphics-avatar-ic-5ba3c66df14d32.3051789815374598219884.jpg";
  File _image;
  String _currentLocation;
  int _currentAge = 0;
  String _selectedValues = 'Nothing to show';
  List<Hobby> _selectedHobbies;

  @override
  void initState() {
    super.initState();
    _selectedHobbies = [];
    _getCurrentLocation();
  }

  _getCurrentLocation() async {
    await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
        .then((Position position) async {
      final coordinates = new Coordinates(
          position.latitude, position.longitude);
      var addresses = await Geocoder.local.findAddressesFromCoordinates(
          coordinates);
      var first = addresses.first;
      setState(() {
        _currentLocation = '${first.locality}';
      });
    }).catchError((e) {
      print(e);
    });
  }

  _getImage() async {
    ImagePicker picker = ImagePicker();
    final pickedFileCamera = await picker.getImage(source: ImageSource.camera);
    final pickedFileGallery = await picker.getImage(
        source: ImageSource.gallery);
    setState(() {
      if (pickedFileCamera != null) {
        _image = File(pickedFileCamera.path);
      } else if (pickedFileGallery != null) {
        _image = File(pickedFileGallery.path);
      } else {
        print('No image selected.');
      }
    });
  }

  _uploadFile() async {
    Dialogs.showLoadingDialog(context, _keyLoader);
    if (_image == null){
      createData(defaultAvatar);
    }else {
      firebase_storage.Reference storageReference = firebase_storage
          .FirebaseStorage.instance
          .ref()
          .child('${Path.basename(_image.path)}');
      firebase_storage.UploadTask uploadTask = storageReference.putFile(_image);
      firebase_storage.TaskSnapshot taskSnapshot = await uploadTask;
      String _uploadedFileURL = await taskSnapshot.ref.getDownloadURL();
      createData(_uploadedFileURL);
    }
  }

  void _showDialog() {
    showDialog<int>(
        context: context,
        builder: (BuildContext context) {
          return new NumberPickerDialog.integer(
            minValue: 0,
            maxValue: 90,
            title: new Text("Select your age"),
            initialIntegerValue: _currentAge,
          );
        }
    ).then((value) =>
    {
      if (value != null) {
        setState(() => _currentAge = value)
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        height: 600,
        child: SingleChildScrollView(
          child: Dialog(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.person, color: Colors.lightBlueAccent,),
                      SizedBox(width: 10.0,),
                      Text("Add User", style: (Theme
                          .of(context)
                          .textTheme
                          .title),),
                    ],
                  ),
                  Divider(),
                  TextField(
                    controller: _userNameController,
                    decoration: InputDecoration(
                      contentPadding: const EdgeInsets.all(16.0),
                      prefixIcon: Container(
                          padding: const EdgeInsets.only(
                              top: 16.0, bottom: 16.0),
                          margin: const EdgeInsets.only(right: 8.0),
                          decoration: BoxDecoration(
                              color: Colors.lightBlueAccent,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(30.0),
                                  bottomLeft: Radius.circular(30.0),
                                  topRight: Radius.circular(30.0),
                                  bottomRight: Radius.circular(10.0)
                              )
                          ),
                          child: Icon(Icons.person, color: Colors.white,)),
                      hintText: "Enter name",
                      hintStyle: TextStyle(color: Colors.black, fontSize: 20.0),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30.0),
                          borderSide: BorderSide.none
                      ),
                      filled: true,
                      fillColor: Colors.white.withOpacity(0.1),
                    ),
                  ),
                  SizedBox(height: 20.0),
                  GestureDetector(
                    onTap: () {
                      _showDialog();
                    },
                    child: new TextField(
                      enabled: false,
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.all(16.0),
                        prefixIcon: Container(
                            padding: const EdgeInsets.only(
                                top: 16.0, bottom: 16.0),
                            margin: const EdgeInsets.only(right: 8.0),
                            decoration: BoxDecoration(
                                color: Colors.lightBlueAccent,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(30.0),
                                    bottomLeft: Radius.circular(30.0),
                                    topRight: Radius.circular(30.0),
                                    bottomRight: Radius.circular(10.0)
                                )
                            ),
                            child: Icon(Icons.person, color: Colors.white,)),
                        hintText: _currentAge == 0 ? "Select age" : _currentAge
                            .toString(),
                        hintStyle: TextStyle(color: Colors.black,
                            fontSize: 20.0),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30.0),
                            borderSide: BorderSide.none
                        ),
                        filled: true,
                        fillColor: Colors.white.withOpacity(0.1),
                      ),
                    ),
                  ),
                  SizedBox(height: 20.0,),
                  Row(
                    children: <Widget>[
                      Icon(Icons.location_on),
                      SizedBox(
                        width: 8,
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Location',
                              style: TextStyle(
                                  color: Colors.grey, fontSize: 14.0),
                            ),
                            if (_currentLocation != null)
                              Text(_currentLocation,
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 20.0)),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 8,
                      ),
                    ],
                  ),
                  SizedBox(height: 20.0),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FlutterTagging<Hobby>(
                      initialItems: _selectedHobbies,
                      textFieldConfiguration: TextFieldConfiguration(
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          filled: true,
                          fillColor: Colors.green.withAlpha(30),
                          hintText: 'Search',
                        ),
                      ),
                      findSuggestions: HobbyService.getHobbies,
                      additionCallback: (value) {
                        print(value);
                        return Hobby(
                          name: value,
                        );
                      },
                      onAdded: (hobby) {
                        // api calls here, triggered when add to tag button is pressed
                        return Hobby();
                      },
                      configureSuggestion: (hob) {
                        return SuggestionConfiguration(
                          title: Text(hob.name),
                          additionWidget: Chip(
                            avatar: Icon(
                              Icons.add_circle,
                              color: Colors.white,
                            ),
                            label: Text('Add New Tag'),
                            labelStyle: TextStyle(
                              color: Colors.white,
                              fontSize: 14.0,
                              fontWeight: FontWeight.w300,
                            ),
                            backgroundColor: Colors.green,
                          ),
                        );
                      },
                      configureChip: (hob) {
                        return ChipConfiguration(
                          label: Text(hob.name),
                          backgroundColor: Colors.blue,
                          labelStyle: TextStyle(color: Colors.white),
                          deleteIconColor: Colors.white,
                        );
                      },
                      onChanged: () {
                        setState(() {
                          _selectedValues =
                              _selectedHobbies.map<String>((hob) => '${hob
                                  .name}').toList().toString();
                        });
                      },
                    ),
                  ),

                  SizedBox(height: 20.0),
                  Container(
                    padding: EdgeInsets.all(5.0),
                    decoration: BoxDecoration(
                        color: Colors.lightBlueAccent,
                        borderRadius: BorderRadius.circular(5.0)
                    ),
                    child: Center(
                      child: _image == null
                          ? Text('No image selected.')
                          : Image.file(_image),
                    ),
                  ),
                  SizedBox(height: 20.0,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      RaisedButton(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8.0, horizontal: 16.0),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0)),
                        onPressed: () {
                          _uploadFile();
                        },
                        color: Colors.lightBlue,
                        textColor: Colors.white,
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text("Upload", style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 16.0
                            ),),
                            const SizedBox(width: 20.0),
                            Container(
                              padding: const EdgeInsets.all(8.0),
                              child: Icon(
                                Icons.upload_file,
                                color: Colors.lightBlueAccent,
                                size: 16.0,
                              ),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius:
                                  BorderRadius.circular(10.0)),
                            )
                          ],
                        ),
                      ),
                      FloatingActionButton(
                        onPressed: _getImage,
                        tooltip: 'Pick Image',
                        child: Icon(Icons.add_a_photo),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void createData(_uploadedFileURL) {
    print(_uploadedFileURL);
    int generateRandomNumber() {
      var random = new Random();
      // Printing Random Number between 1 to 1000000.
      return random.nextInt(1000000);
    }
    var rn = generateRandomNumber();
    var uid = "user-" + rn.toString();
    if (_userNameController.text
        .isEmpty || _currentLocation
        .trim()
        .isEmpty || _uploadedFileURL
        .toString()
        .isEmpty || _selectedValues.isEmpty) {

      Toast.show("Invalid Entries.", context, duration: Toast.LENGTH_LONG,
          gravity: Toast.BOTTOM);
    } else {
      databaseReference.child("Users").child(uid).set({
        'name': _userNameController.text.trim(),
        'age': _currentAge.toString(),
        'location': _currentLocation.trim(),
        'imageURL': _uploadedFileURL,
        'hobbies': _selectedValues
      });

      Toast.show("User uploaded", context, duration: Toast.LENGTH_LONG,
          gravity: Toast.BOTTOM);
    }
    Navigator.of(context, rootNavigator: false).pop();
    Navigator.pop(context);
  }
}

class HobbyService {
  /// Mocks fetching hobby from network API with delay of 500ms.
  static Future<List<Hobby>> getHobbies(String query) async {
    await Future.delayed(Duration(milliseconds: 500), null);
    return <Hobby>[
      Hobby(name: 'Painting'),
      Hobby(name: 'Graphic Design'),
      Hobby(name: 'Community Service'),
      Hobby(name: 'Cooking'),
      Hobby(name: 'Exercising'),
      Hobby(name: 'Outdoor Activities'),
      Hobby(name: 'Playing an Instrument'),
      Hobby(name: 'Team or Individual Sports'),
      Hobby(name: 'Travel'),
      Hobby(name: 'Woodworking'),
      Hobby(name: 'Writing'),
      Hobby(name: 'Listening to Music'),
      Hobby(name: 'Gaming'),
      Hobby(name: 'Art'),
      Hobby(name: 'Nature'),
      Hobby(name: 'Topical Blogs'),
      Hobby(name: 'Theater'),
      Hobby(name: 'History'),
    ]
        .where((hob) => hob.name.toLowerCase().contains(query.toLowerCase()))
        .toList();
  }
}

/// Hobby Class
class Hobby extends Taggable {
  ///
  final String name;

  /// Creates Hobby
  Hobby({
    this.name,
  });
  @override
  List<Object> get props => [name];

}