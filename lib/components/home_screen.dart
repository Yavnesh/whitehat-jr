import 'package:assignment_whitehat_jr/components/authentication_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:provider/provider.dart';
import 'package:firebase_database/firebase_database.dart';

import 'new_user_screen.dart';
import 'user_edit_screen.dart';

class HomeScreenPage extends StatefulWidget {
  HomeScreenPage({Key key, this.user}) : super(key: key);
  final UserCredential user;

  @override
  _HomeScreenPageState createState() =>  _HomeScreenPageState();
}

class _HomeScreenPageState extends State<HomeScreenPage> {

  final dbRef = FirebaseDatabase.instance.reference().child("Users");
  List<Map<dynamic, dynamic>> lists = [];
  List keys = [];

  onGoBack(dynamic value) {
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            backgroundColor: Colors.cyan,
            leading: IconButton(
              icon: Icon(Icons.menu, color: Colors.white,),
              onPressed: (){},
            ),
            title: new Text("Dashboard"),

            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.power_settings_new, color: Colors.white,),
                onPressed: (){
                  context.read<AuthenticationService>().logout();
                },
              ),
            ]
        ),
        body: FutureBuilder(
            future: dbRef.once(),
            builder: (context, AsyncSnapshot<DataSnapshot> snapshot) {
              if (snapshot.hasData) {
                lists.clear();
                keys.clear();
                Map<dynamic, dynamic> values = snapshot.data.value;
                values.forEach((key, value) {
                  keys.add(key);
                });
                values.forEach((key, values) {
                  lists.add(values);
                });

                return Column(
                  children: <Widget>[
                    Expanded( // wrap in Expanded
                      child: lists.length == 0 ?
                      new Container(child: Row( mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                        Text("No Users to display, May be Add one now!!", style: TextStyle(fontSize: 20.0,),)]),) :
                      new ListView.builder(
                          shrinkWrap: true,
                          itemCount: lists.length,
                          itemBuilder: (BuildContext context, int index) {
                            return  usersUI(index);

                          })
                    ),
                  ],
                );
              }
              return CircularProgressIndicator();

        }),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => NewUserScreenPage()),
            ).then(onGoBack);
          },
          tooltip: 'Add new user',
          child: Icon(Icons.add),
        ),
    );
  }

  Widget usersUI(index){
    return GestureDetector(
      onTap: () { print("Container was tapped");
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => UserEditScreenPage(userKey: keys[index].toString(), list: lists[index])),
        ).then(onGoBack);
      },
      child: Container(
        margin: EdgeInsets.only(top: 0.0),
        height: 200.0,
        child: Stack(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(
                  top: 20.0, left: 40.0, right: 40.0, bottom: 10.0),
              child: Material(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                elevation: 5.0,
                color: Colors.white,
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 20.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        const SizedBox(width: 25.0),
                        Material(
                          elevation: 5.0,
                          shape: CircleBorder(),
                          child: CircleAvatar(
                            radius: 40.0,
                            backgroundImage: NetworkImage(lists[index]["imageURL"]),
                          ),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Expanded(
                          child: Container(
                            height: 80.0,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                // renderHobbies(hobbies),
                                Text(
                                  lists[index]["name"],
                                  style: Theme.of(context).textTheme.title,
                                ),
                                SizedBox(height: 5.0,),
                                Text(lists[index]["hobbies"]),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),

                    Container(
                      height: 40.0,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            child: ListTile(
                              title: Text(
                                lists[index]["age"],
                                textAlign: TextAlign.center,
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text("Age".toUpperCase(),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 12.0)),
                            ),
                          ),
                          Expanded(
                            child: ListTile(
                              title: Text(
                                lists[index]["location"],
                                textAlign: TextAlign.center,
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text("Location".toUpperCase(),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 12.0)),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),

      ),
    );
  }
}
