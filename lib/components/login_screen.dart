
import 'package:assignment_whitehat_jr/components/authentication_service.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:wave/config.dart';
import 'package:wave/wave.dart';
import 'package:provider/provider.dart';

import 'forgot_password_screen.dart';
import 'home_screen.dart';
import 'register_screen.dart';

class LoginScreePage extends StatefulWidget {
  @override
  _LoginScreePageState createState() =>  _LoginScreePageState();
  }

class _LoginScreePageState extends State<LoginScreePage> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            height: 650,
            child: RotatedBox(
              quarterTurns: 2,
              child: WaveWidget(
                config: CustomConfig(
                  // gradients: [
                  //   [Colors.deepPurple, Colors.deepPurple.shade200],
                  //   [Colors.indigo.shade200, Colors.purple.shade200],
                  // ],
                  gradients: [
                    [Colors.blue.shade800, Colors.lightBlue.shade500],
                    [Colors.lightBlue.shade200, Colors.blue.shade500],
                  ],
                  durations: [19440, 10800],
                  heightPercentages: [0.20, 0.25],
                  blur: MaskFilter.blur(BlurStyle.solid, 10),
                  gradientBegin: Alignment.bottomLeft,
                  gradientEnd: Alignment.topRight,
                ),
                waveAmplitude: 0,
                size: Size(
                  double.infinity,
                  double.infinity,
                ),
              ),
            ),
          ),
          ListView(
            children: <Widget>[
              Form(
                key: _formKey,
                child: Container(
                  height: 400,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Card(
                        margin: EdgeInsets.only(left: 30, right:30, top:30),
                        elevation: 11,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(40))),
                        child: TextFormField(
                          controller: _emailController,
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.person, color: Colors.lightBlueAccent,),
                              // suffixIcon: Icon(Icons.check_circle, color: Colors.black26,),
                              hintText: "Email",
                              hintStyle: TextStyle(color: Colors.black26,
                                fontSize: 20.0,),
                              filled: true,
                              fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.all(Radius.circular(40.0)),
                              ),
                              contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 16.0)
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter some text';
                            }else{
                              return null;
                            }
                          },
                        ),
                      ),
                      Card(
                        margin: EdgeInsets.only(left: 30, right:30, top:20),
                        elevation: 11,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(40))),
                        child: TextFormField(
                          controller: _passwordController,
                          obscureText: true,
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.lock, color: Colors.lightBlueAccent,),
                              hintText: "Password",
                              hintStyle: TextStyle(
                                color: Colors.black26,
                                fontSize: 20.0,
                              ),
                              filled: true,
                              fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.all(Radius.circular(40.0)),
                              ),
                              contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 16.0)
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter some text';
                            }else{
                              return null;
                            }
                          },
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        padding: EdgeInsets.all(30.0),
                        child: RaisedButton(
                          padding: EdgeInsets.symmetric(vertical: 16.0),
                          color: Colors.blue.shade800,
                          onPressed: () {
                            context.read<AuthenticationService>().login(
                              email: _emailController.text.trim(),
                              password: _passwordController.text.trim(),
                            ).then((value) => {
                              if(value == "success"){
                                Toast.show ("Successfully Logged In", context,
                                    duration: Toast.LENGTH_LONG,
                                    gravity: Toast.BOTTOM),
                                // Navigate to Home Page
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => HomeScreenPage()),
                                )
                              }else{
                                Toast.show ("Invalid Credentials", context,
                                    duration: Toast.LENGTH_LONG,
                                    gravity: Toast.BOTTOM),
                              },
                            });
                          },
                          elevation: 11,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(40.0))),
                          child: Text("Login", style: TextStyle(
                              color: Colors.white70,
                            fontSize: 20.0,
                          )),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => ForgotPasswordScreenPage()),
                          );
                        },
                        child: new Text("Forgot your password?", style: TextStyle( color: Colors.white)),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(height: 100,),

              Align(
                alignment: Alignment.bottomCenter,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        SizedBox(width: 20.0,),
                        Expanded(
                          child: RaisedButton(
                            padding: EdgeInsets.symmetric(vertical: 16.0),
                            color: Colors.blue.shade800,
                            onPressed: (){
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => RegisterScreenPage()),
                              );
                            },
                            elevation: 11,
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(40.0))),
                            child: Text("Register", style: TextStyle(
                                color: Colors.white70,
                              fontSize: 20.0
                            )),
                          ),
                        ),
                        SizedBox(width: 20.0,),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}