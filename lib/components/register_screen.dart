import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';
import 'authentication_service.dart';
import 'home_screen.dart';

final FirebaseAuth auth = FirebaseAuth.instance;

class RegisterScreenPage extends StatefulWidget {
  final String title = 'Registration';
  @override
  State<StatefulWidget> createState() =>
      _RegisterScreenPageState();
}

class _RegisterScreenPageState extends State<RegisterScreenPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const SizedBox(height: 100.0),
              Stack(
                children: <Widget>[
                  Positioned(
                    left: 20.0,
                    top: 15.0,
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.lightBlueAccent,
                          borderRadius: BorderRadius.circular(20.0)
                      ),
                      width: 70.0,
                      height: 20.0,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 32.0),
                    child: Text(
                      "Register",
                      style:
                      TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 30.0),
              Padding(
                padding:
                const EdgeInsets.symmetric(horizontal: 32, vertical: 8.0),
                child: TextFormField(
                  controller: _emailController,
                  decoration: InputDecoration(
                      prefixIcon: Icon(Icons.person, color: Colors.lightBlueAccent,),
                      // suffixIcon: Icon(
                      //   Icons.check_circle, color: Colors.black26,),
                      hintText: "Email",
                      hintStyle: TextStyle(color: Colors.black26,
                        fontSize: 20.0,),
                      filled: true,
                      fillColor: Colors.white,
                      border: OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius: BorderRadius.all(Radius.circular(40.0)),
                      ),
                      contentPadding: EdgeInsets.symmetric(
                          horizontal: 20.0, vertical: 16.0)
                  ),
                ),
              ),
              Padding(
                padding:
                const EdgeInsets.symmetric(horizontal: 32, vertical: 8.0),
                child: TextFormField(
                  controller: _passwordController,
                  obscureText: true,
                  decoration: InputDecoration(
                      prefixIcon: Icon(Icons.lock_rounded, color: Colors.lightBlueAccent,),
                      // suffixIcon: Icon(
                      //   Icons.check_circle, color: Colors.black26,),
                      hintText: "Password",
                      hintStyle: TextStyle(color: Colors.black26,
                      fontSize: 20.0,),
                      filled: true,
                      fillColor: Colors.white,
                      border: OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius: BorderRadius.all(Radius.circular(40.0)),
                      ),
                      contentPadding: EdgeInsets.symmetric(
                          horizontal: 20.0, vertical: 16.0)
                  ),
                ),
              ),
              const SizedBox(height: 120.0),
              Align(
                alignment: Alignment.centerRight,
                child: RaisedButton(
                  padding: const EdgeInsets.fromLTRB(40.0, 16.0, 30.0, 16.0),
                  color: Colors.lightBlueAccent,
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30.0),
                          bottomLeft: Radius.circular(30.0))),
                  onPressed: () {
                    context.read<AuthenticationService>().register(
                      email: _emailController.text.trim(),
                      password: _passwordController.text.trim(),
                    ).then((value) => {
                    if(value == "success"){
                        Toast.show ("Successfully Registered", context,
                        duration: Toast.LENGTH_LONG,
                        gravity: Toast.BOTTOM),
                        // Navigate to Home Page
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => HomeScreenPage()),
                        )
                      }else{
                      Toast.show ("Invalid email or Password Should be minimum 6 digits", context,
                          duration: Toast.LENGTH_LONG,
                          gravity: Toast.BOTTOM),
                      },
                    });
                  },

                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                        Text(
                          "Sign Up".toUpperCase(),
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20.0),),
                      const SizedBox(width: 40.0),
                      Icon(
                        FontAwesomeIcons.arrowRight,
                        size: 18.0,
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}