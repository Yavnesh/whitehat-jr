import 'package:assignment_whitehat_jr/components/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';
import 'authentication_service.dart';

class ForgotPasswordScreenPage extends StatelessWidget {
  final Color color1 = Colors.blue.shade800;
  final Color color2 = Colors.blue.shade500;
  final Color color3 = Colors.blue.shade100;

  final TextEditingController _emailController = TextEditingController();

  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _buildHeader(),
            SizedBox(height: 40.0),
            Container(
              height: 50,
              padding: const EdgeInsets.only(left: 40.0),
              child: OverflowBox(
                maxWidth: 500,
                alignment: Alignment.centerLeft,
                child: Column(
                  children: <Widget>[
                    Text("Enter the email address\nassociated with your account.", style: TextStyle(
                        color: Colors.black,
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold
                    ),),
                  ],
                ),
              ),
            ),
            SizedBox(height: 20.0),
            Padding(
              padding:
              const EdgeInsets.symmetric(horizontal: 32, vertical: 8.0),
              child: TextField(
                controller: _emailController,
                decoration: InputDecoration(
                    labelText: "Email", hasFloatingPlaceholder: true),
              ),
            ),
            SizedBox(height: 30,),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 80.0),
              width: double.infinity,
              child: RaisedButton(
                padding: const EdgeInsets.all(16.0),
                textColor: Colors.white,
                color: Colors.blue.shade800,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(40.0)),
                onPressed: () {
                  context.read<AuthenticationService>().send_password_reset_email(
                    email: _emailController.text.trim(),
                  );
                  Toast.show("A password reset link has been sent to " + _emailController.text, context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
                  // Navigate to Login Page
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => LoginScreePage()),
                  );
                },
                child: Text(
                  "Reset Password",
                  style:
                  TextStyle(fontWeight: FontWeight.w300, fontSize: 18.0),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Container _buildHeader() {
    return Container(
      height: 250,
      width: double.infinity,
      child: Stack(
        children: <Widget>[
          Positioned(
            bottom: 0,
            left: -100,
            top: -150,
            child: Container(
              width: 350,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(
                      colors: [color1, color2]
                  ),
                  boxShadow: [
                    BoxShadow(
                        color: color2,
                        offset: Offset(4.0,4.0),
                        blurRadius: 10.0
                    )
                  ]
              ),
            ),
          ),
          Container(
            width: 100,
            height: 100,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                gradient: LinearGradient(
                    colors: [color3,color2]
                ),
                boxShadow: [
                  BoxShadow(
                      color: color3,
                      offset: Offset(1.0,1.0),
                      blurRadius: 4.0
                  )
                ]
            ),
          ),
          Positioned(
            top: 100,
            right: 200,
            child: Container(
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(
                      colors: [color3,color2]
                  ),
                  boxShadow: [
                    BoxShadow(
                        color: color3,
                        offset: Offset(1.0,1.0),
                        blurRadius: 4.0
                    )
                  ]
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(
                top: 60,
                left: 30
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("Reset", style: TextStyle(
                    color: Colors.white,
                    fontSize: 45.0,
                    fontWeight: FontWeight.w700
                ),),
                SizedBox(height: 10.0),
                Text("Password?", style: TextStyle(
                    color: Colors.white,
                    fontSize: 45.0,
                    fontWeight: FontWeight.w700
                ),)
              ],
            ),
          )
        ],
      ),
    );
  }
}